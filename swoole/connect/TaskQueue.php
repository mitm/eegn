<?php
// use Swoole\Redis\Server;
// use SwooleCoroutineRedis;
class TaskQueue
{
    private $client;
    private $redisClient;
    private $mysqlClient;

    public function __construct()
    {
        // 初始化 Redis 客户端
        // $server = new Server("127.0.0.1", 9501, SWOOLE_BASE);
        // $this->client= new swoole_client(SWOOLE_SOCK_TCP);
        $this->redisClient = new \Swoole\Client(SWOOLE_SOCK_TCP);
        // $this->redisClient = new Redis();
        $this->redisClient->connect('127.0.0.1', 6379);
        // $this->redisClient->auth('xxxx');

        // 初始化 MySQL 客户端
        // $this->mysqlClient = new mysqli('127.0.0.1', 'username', 'password', 'database_name');
        // if ($this->mysqlClient->connect_errno) {
        //     echo("Failed to connect to MySQL: (" . $this->mysqlClient->connect_errno . ") " . $this->mysqlClient->connect_error);
        // }
    }

    public function pushTask($task)
    {
        // 将任务添加到 Redis 队列中
        // $this->redisClient->lPush('task_queue', json_encode($task));
        $this->redisClient->lPush('task_queue', $task);
    }

    public function run()
    {
        // 监听 Redis 队列
        while (true) {
            $taskJson = $this->redisClient->rPop('task_queue');
            if (!empty($taskJson)) {
                // 解析任务
                $task = json_decode($taskJson, true);

                // 执行任务
                $this->processTask($task);
            }
        }
    }

    private function processTask($task)
    {
        // 将任务状态设置为正在执行
        // $this->updateTaskStatus($task['id'], 1);

        // // 执行导出任务
        // // ...

        // // 更新任务状态为已完成
        // $this->updateTaskStatus($task['id'], 2);

        // 保存任务结果到数据库
        $this->saveTaskResult($task);
    }

    private function updateTaskStatus($taskId, $status)
    {
        // 更新任务状态到 MySQL 数据库中
        $sql = "UPDATE t_report_task SET checked={$status} WHERE id={$taskId}";
        $this->mysqlClient->query($sql);
    }

    private function saveTaskResult($data)
    {
        $conn = MysqlPool::getInstance()->getConn();
        // var_dump("SELECT * FROM koa_form_msg WHERE gridKey=".$key);
        // var_dump($key);
        $m_data=$conn->query("SELECT * FROM koa_form_msg WHERE gridKey='$key'");
        $info = json_decode($m_data[0]['data']);
        if($info){
            $data['v']['m'] = $data['v']['v'];
            $info[$data['r']][$data['c']]=$data['v'];
            $info=json_encode($info);
            // UPDATE student SET name = '张三丰', age = 20 WHERE id = 1
            var_dump($info);
            $res=$conn->query("UPDATE koa_form_msg SET data ='$info' WHERE gridKey ='$key'");
            var_dump($res);
        }
        MysqlPool::getInstance()->recycle($conn);
        // 保存任务结果到 MySQL 数据库中
        // $sql = "UPDATE t_report_task SET url='{$task['url']}', file_info='{$task['file_info']}', file_size={$task['file_size']}, finish_time=NOW() WHERE id={$task['id']}";
        // $this->mysqlClient->query($sql);
    }
}