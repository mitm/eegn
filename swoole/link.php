<?php 

// use connect\TaskQueue;
use Swoole\Coroutine\Channel;

use Swoole\Coroutine\MySQL;

class MysqlPool{ 
    private $min;// 最小连接数
    
    private $max;// 最大连接数
    
    private $count;// 当前连接数
    
    private $connections;// 连接池
    
    protected $freeTime;// 用于空闲连接回收判断
    
    public static $instance;

    /**
    
    * MysqlPool constructor.
    
    */
    
    public function __construct()
    
    {
        $this->min = 10;
        
        $this->max = 100;
        
        $this->freeTime = 10 * 3600;
        
        $this->connections = new Channel($this->max + 1);
    
    }

    /**
    
    * @return MysqlPool
    
    */
    
    public static function getInstance() {
        if (is_null(self::$instance)) {
        self::$instance = new self();
    
    }

    return self::$instance;}

    /**
    
    * 创建连接
    
    * @return MySQL
    
    */ 
    protected function createConnection() {
        $conn = new MySQL();
        
        $conn->connect([
        
        'host' => '127.0.0.1',
        
        'port' => '9090',
        
        'user' => 'oa_maer_pro',
        
        'password' => 'EcyiMywJweaxfHYX',
        
        'database' => 'oa_maer_pro',
        
        'timeout' => 5 ]);
        
        return $conn; 
        
    }

    /**
    
    * 创建连接对象
    
    * @return array|null
    
    */
    
    protected function createConnObject() {
        $conn = $this->createConnection();
    
        return $conn ? ['last_used_time' => time(), 'conn' => $conn] : null; 
        
    }

    /**
    
    * 初始化连接
    
    * @return $this
    
    */
    
    public function init() {
        for ($i = 0; $i < $this->min; $i++) {
        $obj = $this->createConnObject();
        
        $this->count++;
        
        $this->connections->push($obj);
        
        }
        
        return $this;
    
    }

    /**
    
    * 获取连接
    
    * @param int $timeout
    
    * @return mixed
    
    */
    
    public function getConn($timeout = 3) {
        if ($this->connections->isEmpty()) {
        if ($this->count < $this->max) {
        $this->count++;
        
        $obj = $this->createConnObject();
        
        } else {
        $obj = $this->connections->pop($timeout);
        
        }
        
        } else {
        $obj = $this->connections->pop($timeout);
        
        }
        
        return $obj['conn']->connected ? $obj['conn'] : $this->getConn(); 
        
    }

    /**
    
    * 回收连接
    
    * @param $conn
    
    */
    
    public function recycle($conn)
    
    {
        if ($conn->connected) {
        $this->connections->push(['last_used_time' => time(), 'conn' => $conn]);
        
        }
    
    }

    /**
    
    * 回收空闲连接
    
    */
    
    public function recycleFreeConnection()
    
    {
    // 每 2 分钟检测一下空闲连接
    
    swoole_timer_tick(2 * 60 * 1000, function () {
        if ($this->connections->length() < intval($this->max * 0.5)) {
        // 请求连接数还比较多，暂时不回收空闲连接
        
        return;
        
        }
        
        while (true) {
            if ($this->connections->isEmpty()) {
                break;
            
            }
            
            $connObj = $this->connections->pop(0.001);
            
            $nowTime = time();
            
            $lastUsedTime = $connObj['last_used_time'];
            
            // 当前连接数大于最小的连接数，并且回收掉空闲的连接
            
            if ($this->count > $this->min && ($nowTime - $lastUsedTime > $this->freeTime))
            
            {
                $connObj['conn']->close();
                
                $this->count--;
            
            } else {
                $this->connections->push($connObj);
            
            }
        
        }
    
    });
    
}}
use Swoole\Coroutine\Redis;
class TaskQueue
{
    private $client;
    private $redisClient;
    private $mysqlClient;

    public function __construct()
    {
        // 初始化 Redis 客户端
        // $server = new Server("127.0.0.1", 9501, SWOOLE_BASE);
        // $this->client= new swoole_client(SWOOLE_SOCK_TCP);
        // $this->redisClient = new \Swoole\Client(SWOOLE_SOCK_TCP);
        $this->redisClient = new Redis();
        $this->redisClient->connect('127.0.0.1', 6379);
        // $this->redisClient->auth('xxxx');

        // 初始化 MySQL 客户端
        // $this->mysqlClient = new mysqli('127.0.0.1', 'username', 'password', 'database_name');
        // if ($this->mysqlClient->connect_errno) {
        //     echo("Failed to connect to MySQL: (" . $this->mysqlClient->connect_errno . ") " . $this->mysqlClient->connect_error);
        // }
    }
    
     public function setData($key,$data)
    {
        // echo 1;
       return $this->redisClient->set($key,serialize($data));
    }
    
     public function getData($key)
    {
        // echo 2;
       return unserialize($this->redisClient->get($key));
    }
    
     public function delData($key)
    {
        // echo 3;
       $this->redisClient->del($key);
    }
        
    public function pushTask($task)
    {
        // var_dump(1);
        // 将任务添加到 Redis 队列中
        $this->redisClient->lPush('task_queue', json_encode($task));
        // $this->redisClient->lPush('task_queue', $task);
        // var_dump(2);
    }

    public function run()
    {
        // 监听 Redis 队列
        while (true) {
            $taskJson = $this->redisClient->rPop('task_queue');
            if (!empty($taskJson)) {
                // 解析任务
                $task = json_decode($taskJson, true);
                // 执行任务
                $this->processTask($task);
            }
        }
    }

    private function processTask($task)
    {
        // 将任务状态设置为正在执行
        // $this->updateTaskStatus($task['id'], 1);

        // // 执行导出任务
        // // ...

        // // 更新任务状态为已完成
        // $this->updateTaskStatus($task['id'], 2);

        // 保存任务结果到数据库
        $this->saveTaskResult($task);
    }

    private function updateTaskStatus($taskId, $status)
    {
        // 更新任务状态到 MySQL 数据库中
        $sql = "UPDATE t_report_task SET checked={$status} WHERE id={$taskId}";
        $this->mysqlClient->query($sql);
    }

    private function saveTaskResult($data)
    {
        $conn = MysqlPool::getInstance()->getConn();
        // var_dump("SELECT * FROM koa_form_msg WHERE gridKey=".$key);
        // var_dump($key);
        $gridKey=$data['key'];
        $m_data=$conn->query("SELECT * FROM koa_form_msg WHERE gridKey='$gridKey'");
        var_dump(3);
        var_dump($gridKey);
        // $info = json_decode($m_data[0]['data']);
        $info = $m_data[0]['data'];
        if($info){
            $data['v']['m'] = $data['v']['v'];
            $info[$data['r']][$data['c']]=$data['v'];
            $info=json_encode($info);
            // UPDATE student SET name = '张三丰', age = 20 WHERE id = 1
            var_dump(4);
            $res=$conn->query("UPDATE koa_form_msg SET data ='$info' WHERE gridKey ='$key'");
            var_dump(5);
        }
        MysqlPool::getInstance()->recycle($conn);
        // 保存任务结果到 MySQL 数据库中
        // $sql = "UPDATE t_report_task SET url='{$task['url']}', file_info='{$task['file_info']}', file_size={$task['file_size']}, finish_time=NOW() WHERE id={$task['id']}";
        // $this->mysqlClient->query($sql);
    }
}

// global $groups;  
// global $users;
//创建WebSocket Server对象，监听0.0.0.0:9502端口
$ws = new Swoole\WebSocket\Server('0.0.0.0', 88);
// $ws->set(['work_num' => 200]);
// 配置参数
    $ws ->set([
	'daemonize' => true, //守护进程化
	'dispatch_mode'=>5,
// 	'max_wait_time' => 120,
    'log_file'=>'./swebsocket.log',
	'worker_num'=>3,
    'reload_async' => true,
	'heartbeat_idle_time'=>600,
	'heartbeat_check_interval'=>60,
	'max_connection'=>100,
	//配置SSL证书和密钥路径
	'ssl_cert_file' => "/www/wwwroot/gateway.maer.pro/swoole/fullchain.pem",
	'ssl_key_file'  => "/www/wwwroot/gateway.maer.pro/swoole/privkey.key"
    ]);

// $ws->on('WorkerStart', function ($ws, $workerId) {
//     // // 每个 Worker 进程都实例化一个 TaskQueue 对象
//     // $taskQueue = new TaskQueue();
//     // // 启动任务队列
//     // $taskQueue->run();
//     // var_dump($workerId);
// });
//监听WebSocket连接打开事件
$ws->on('Open', function ($ws, $request) {
    // MysqlPool::getInstance()->init()->recycleFreeConnection();
    // var_dump($request);
    $fd=$request->fd;
    echo "client-{$fd} is connected";
    $gridKey=$request->get['g'];
    $user_info=json_decode($request->get['username'],true);
    $ip=$request->header['remoteip'];
    $user_agent=$request->header['user-agent'];
    $sheet=new TaskQueue();
    $users=$sheet->getData('users');
    $groups=$sheet->getData('groups');
    $users[$fd]=$gridKey;
    $groups[$gridKey][$fd]=array('username'=>$user_info[0],'ip'=>$ip,'user_id'=>$user_info[1]);
    $groups[$gridKey][$fd]['user_agent']=substr($user_agent, 0, 255);
    $sheet->setData('users',$users);
    $sheet->setData('groups',$groups);
});
//监听WebSocket消息事件
$ws->on('Message', function ($ws, $frame) {
    $sheet=new TaskQueue();
    $users=$sheet->getData('users');
    $groups=$sheet->getData('groups');
    // var_dump($users);var_dump($groups);
    $gridKey=$users[$frame->fd];
    $data=json_decode($frame->data,true);
    $type=2; $status=0; $time=time();
    $user_id  = $groups[$users[$frame->fd]][$frame->fd]['user_id'];
    $username  = $groups[$users[$frame->fd]][$frame->fd]['username'];
    $ip = $groups[$users[$frame->fd]][$frame->fd]['ip'];
    $user_agent = $groups[$users[$frame->fd]][$frame->fd]['user_agent'];
    if($data['t']=='v'){
        $conn = MysqlPool::getInstance()->getConn();
        $m_data=$conn->query("SELECT * FROM koa_form_msg WHERE gridKey='$gridKey'");
        $info = json_decode($m_data[0]['data']);
        // $info = $m_data[0]['data'];
        if($info){
            $data['v']['m'] = $data['v']['v'];
            $info[$data['r']][$data['c']]=$data['v'];
            $info=json_encode($info,JSON_UNESCAPED_UNICODE);
            // UPDATE student SET name = '张三丰', age = 20 WHERE id = 1
            $res=$conn->query("UPDATE koa_form_msg SET data ='$info' WHERE gridKey ='$gridKey'");
            $ret=$conn->query("INSERT INTO koa_admin_log(title,content,url,admin_id,username,useragent,ip,createtime) VALUES('修改数据填报数据','[]','/api/s1/Report/saveMsg',$user_id,'$username','$user_agent','$ip',$time)");
            MysqlPool::getInstance()->recycle($conn);
        }
    }elseif ($data['t']=='mv') {
         $type=3; $status=1;
    }
    // elseif($data['t']=='na'){
        
    // }
    elseif($data['t']=='rv'){
        $conn = MysqlPool::getInstance()->getConn();
        $m_data=$conn->query("SELECT * FROM koa_form_msg WHERE gridKey='$gridKey'");
        // echo 'key';
        // var_dump($gridKey);
        // echo 'm数据呀';
        // var_dump($m_data);
        $info = json_decode($m_data[0]['data']);
        $i=$data['range']['row'][0];
        $length=$data['range']['column'][1]-$data['range']['column'][0]+1;
        $rows=$data['v'];
        foreach ($rows as $value){
            // echo '数据';
            // var_dump($value);
            // echo 'info';
            if(!empty($value)){
                // var_dump($info[$i]);
                array_splice($info[$i],$data['range']['column'][0],$length,$value);
            }
            $i++;
        }
        $info=json_encode($info,JSON_UNESCAPED_UNICODE);
        // var_dump($info);   
        $res=$conn->query("UPDATE koa_form_msg SET data ='$info' WHERE gridKey ='$gridKey'");
        $ret=$conn->query("INSERT INTO koa_admin_log(title,content,url,admin_id,username,useragent,ip,createtime) VALUES('修改数据填报数据','[]','/api/s1/Report/saveMsg',$user_id,'$username','$user_agent','$ip',$time)");
        MysqlPool::getInstance()->recycle($conn);
    }
    if($frame->data!='rub'){
        $fromfd=$frame->fd;
        $sheet=new TaskQueue();
        $users=$sheet->getData('users');
        $groups=$sheet->getData('groups');
        // // echo 'mes';
        // var_dump($users);var_dump($groups);
        if(!empty($groups[$users[$fromfd]])){
            $stt_data=[
            'createTime'=>$time,
            'id'=>$fromfd,
            'data'=>json_decode($frame->data),
            'returnMessage'=> 'success',
            'status'=>$status,
            'type'=>$type,
            'username'=>'admin',
            ];
            $stt_data['username']=$groups[$users[$fromfd]][$fromfd]['username'];
            foreach ($groups[$users[$fromfd]] as $fd=>$user){
                // $ws_connect=array($ws->connections);
                // if(!in_array($fd,$ws_connect)){
                //     continue;
                // }
                if($fd==$fromfd){
                        continue;
                }
                $sttjson=json_encode($stt_data);
                $ws->push($fd, $sttjson);
            }
        }
    }else{
        echo '心跳包:'.date('H:i:s',$time).$frame->data;
    }
    // $ws->push($frame->fd, "$stt");
});

//监听WebSocket连接关闭事件
$ws->on('Close', function ($ws, $fd) {
    $sheet=new TaskQueue();
    $users=$sheet->getData('users');
    $groups=$sheet->getData('groups');
    // echo 'on111';
    // var_dump($users);var_dump($groups);
    if($groups[$users[$fd]][$fd]){
        unset($groups[$users[$fd]][$fd]);
        if(empty($groups[$users[$fd]])){
            unset($groups[$users[$fd]]);
        }
        $sheet->setData('groups',$groups);
    }
    if($users[$fd]){
        unset($users[$fd]);
    }
    $sheet->setData('users',$users);
    //  echo 'on222';
    // var_dump($users);var_dump($groups);
    echo "client-{$fd} is closed";
});

$ws->start();